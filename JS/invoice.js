jQuery.noConflict();
(function ($) {
    "use strict";

    // ロケールを設定
    moment.locale('ja');

    class RecordField {
        constructor(productCode, productName, taxClass, unitPrice, quantity, price) {
            this.productCode = productCode;
            this._productName = productName;
            this.taxClass = taxClass;
            this._unitPrice = unitPrice;
            this.quantity = quantity;
            this._price = price;
        }
        get productName() {
            if (this.taxClass == '非課税') {
                return this._productName + '(非課税)';
            }else if(this.taxClass == '8%'){
                return this._productName + '(※)';
            }
            return this._productName;
        }
        get unitPrice() {
            var formatter = new Intl.NumberFormat('ja-JP', {
                style: 'currency',
                currency: 'JPY'
            });
            return formatter.format(this._unitPrice);
        }
        get price() {
            var formatter = new Intl.NumberFormat('ja-JP', {
                style: 'currency',
                currency: 'JPY'
            });
            return formatter.format(this._price);
        }
    }

    const calcAmount = (unitPrice, quantity) => {
        return Number.parseFloat(unitPrice) * Number.parseFloat(quantity);
    }

    const calcSubtotal = (record) => {
        let price_total = 0;
        for (let i = 0; i < record.Table.value.length; i++) {
            price_total += Number.parseFloat(record.Table.value[i].value.price.value);
        }
        for (let i = 0; i < record.Table_2.value.length; i++) {
            price_total += Number.parseFloat(record.Table_2.value[i].value.price_2.value);
        }
        record.subtotal.value = price_total;
    }

    const calcTax = (rec) => {
        let eightParcentAmount = 0;
        let tenParcentAmount = 0;
        for (let i = 0; i < rec.Table.value.length; i++) {
            if (rec.Table.value[i].value.taxClass.value == "非課税") {
                continue;
            }
            // 8%
            if (rec.Table.value[i].value.taxClass.value == "8%") {
                eightParcentAmount += Number.parseFloat(rec.Table.value[i].value.price.value);
            } else {
                // 10%
                tenParcentAmount += Number.parseFloat(rec.Table.value[i].value.price.value);
            }
        }
        for (let j = 0; j < rec.Table_2.value.length; j++) {
            if (rec.Table_2.value[j].value.taxClass_2.value == "非課税") {
                continue;
            }
            // 8%
            if (rec.Table_2.value[j].value.taxClass_2.value == "8%") {
                eightParcentAmount += Number.parseFloat(rec.Table_2.value[j].value.price_2.value);
            } else {
                // 10%
                tenParcentAmount += Number.parseFloat(rec.Table_2.value[j].value.price_2.value);
            }
        }
        // 小数点以下は四捨五入
        rec.tax_8.value = Math.round(eightParcentAmount * 0.08);
        rec.tax_10.value = Math.round(tenParcentAmount * 0.1);
    }

    // 印刷画面が表示されたときに動作
    kintone.events.on('app.record.print.show', function (event) {
        // 既存html削除
        $('#record-gaia > div.layout-gaia').empty();
        // テンプレートhtml追加
        $('#record-gaia > div.layout-gaia').append(invoice.normal.htmltag);
        // テンプレートCSS追加
        $('head').append(invoice.normal.style);
        // データテキスト調整用CSS追加
        // $('head').append(invoice.normal.textStyle);
        // 印刷領域の色とbodyの背景色を揃える
        $('#record-gaia').css({ 'background-color': '#FFFFFF' });
        $('#record-gaia > div.layout-gaia').width("");
        $('#record-gaia > div.layout-gaia').css({ 'background-color': '#FFFFFF' });
        $('body').css({ 'background-color': '#FFFFFF' });
        // レコード取得
        var rec = event.record;
        console.log(rec);
        //  明細テーブル（商品マスタ）
        var records = [];
        for (let i = 0; i < rec.Table.value.length; i++) {
            if (rec.Table.value[i].value.quantity.value == "0") {
                continue;
            }
            records.push(
                new RecordField(
                    rec.Table.value[i].value.productCode.value,
                    rec.Table.value[i].value.productName.value,
                    rec.Table.value[i].value.taxClass.value,
                    rec.Table.value[i].value.unitPrice.value,
                    rec.Table.value[i].value.quantity.value,
                    rec.Table.value[i].value.price.value
                )
            );
        }

        //  明細テーブル（個別）
        var records_2 = [];
        for (let j = 0; j < rec.Table_2.value.length; j++) {
            if (rec.Table_2.value[j].value.quantity_2.value == "0") {
                continue;
            }
            records_2.push(
                new RecordField(
                    '',
                    rec.Table_2.value[j].value.productName_2.value,
                    rec.Table_2.value[j].value.taxClass_2.value,
                    rec.Table_2.value[j].value.unitPrice_2.value,
                    rec.Table_2.value[j].value.quantity_2.value,
                    rec.Table_2.value[j].value.price_2.value
                )
            );
        }


        new Vue({
            el: "#App01",
            data: {
                recordid: rec.recordid.value,
                billingCompanyName: rec.billingCompanyName.value,
                billingRepName: rec.billingRepName.value,
                ownCompanyName: rec.ownCompanyName.value,
                ownCompanyRepName: rec.ownCompanyRepName.value,
                ownCompanyPostalCode: rec.ownCompanyPostalCode.value,
                ownCompanyAddress: rec.ownCompanyAddress.value,
                ownCompanyTel: rec.ownCompanyTel.value,
                subject: rec.subject.value,
                billingDate: rec.billingDate.value,
                ownCompanyMail: rec.ownCompanyMail.value,
                transferDeadline: rec.transferDeadline.value,
                bankName: rec.bankName.value,
                branchName: rec.branchName.value,
                bankAccountNumber: rec.bankAccountNumber.value,
                accountName: rec.accountName.value,
                accountType: rec.accountType.value,
                comment: `${rec.comment.value}`,
                subtotal: rec.subtotal.value,
                tax_8: rec.tax_8.value,
                tax_10: rec.tax_10.value,
                ownCompanyFax: rec.ownCompanyFax.value,
                billingPriceTotal: rec.billingPriceTotal.value,
                tableField: records,
                tableField_2: records_2,
                companySealData: rec.companySeal.value,
                companySealStyle: {
                    backgroundImage: 'none',
                    backgroundRepeat: 'no-repeat',
                    backgroundAttachment: 'local',
                    backgroundPosition: 'top 100px right 10px',
                    backgroundSize: '100px'
                }
            },
            computed: {
                getBillingPriceTotal: function () {
                    var formatter = new Intl.NumberFormat('ja-JP', {
                        style: 'currency',
                        currency: 'JPY'
                    });
                    return formatter.format(this.billingPriceTotal);
                },
                getSubtotal: function () {
                    var formatter = new Intl.NumberFormat('ja-JP', {
                        style: 'currency',
                        currency: 'JPY'
                    });
                    return formatter.format(this.subtotal);
                },
                getTax_8: function () {
                    var formatter = new Intl.NumberFormat('ja-JP', {
                        style: 'currency',
                        currency: 'JPY'
                    });
                    return formatter.format(this.tax_8);
                },
                getTax_10: function () {
                    var formatter = new Intl.NumberFormat('ja-JP', {
                        style: 'currency',
                        currency: 'JPY'
                    });
                    return formatter.format(this.tax_10);
                },
                getBillingDate: function () {
                    return moment(this.billingDate).format('YYYY/MM/DD');
                },
                getTransferDeadline: function () {
                    return moment(this.transferDeadline).format('YYYY/MM/DD');
                },
                getComment: function () {
                    return this.comment.replace(/\n/g, '<br>');
                }
            },
            methods: {
                /**
                 * 社判画像をダウンロードしてCSSを更新
                 * @param {string} filekey 
                 */
                updateCompanyImg: function (filekey) {
                    var that = this;
                    var apiurl = '/k/v1/file.json?fileKey=' + filekey;
                    var xhr = new XMLHttpRequest();
                    xhr.open('GET', apiurl, true);
                    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                    xhr.responseType = "blob";
                    var blob = xhr.responseType;
                    xhr.onload = function () {
                        //blobからURL生成
                        var blob = xhr.response;
                        var url = window.URL || window.webkitURL;
                        var image = url.createObjectURL(blob);
                        that.companySealStyle.backgroundImage = 'url(' + image + ')';
                    };
                    xhr.send();
                }
            },
            mounted: function () {
                // 社判画像ダウンロードCSS更新
                if (this.companySealData.length >= 1) {
                    this.updateCompanyImg(this.companySealData[0].fileKey);
                }
            }
        });

    });

    /**
     * 明細テーブル（商品マスタ）金額計算
     */
    kintone.events.on(['app.record.create.show', 'app.record.edit.show'
    ], function (event) {
        let rec = event.record;
        // アプリアクションボタンで追加画面を表示してるか判定
        if((/action/).test(window.location.search)){
            // 文頭?を除外
            let queryStr = window.location.search.slice(1);
            let queries = {};
            // クエリ文字列を & で分割して処理
            queryStr.split('&').forEach(function(query) {
                // = で分割してkey,valueをオブジェクトに格納
                let queryArr = query.split('=');
                queries[queryArr[0]] = queryArr[1];
            });
            // アプリアクションボタンによる追加画面表示時
            if(queries['action'] === '5526947'){
                console.log('action exe!!');
                rec['Table_2'].value.forEach((subRec) => {
                    subRec.value.price_2.value = calcAmount(subRec.value.unitPrice_2.value, subRec.value.quantity_2.value);
                });
                // ルックアップ項目自動取得
                rec['billingCompanyName'].lookup = true;
            }
        }
        // 小計計算
        calcSubtotal(rec);
        // 消費税計算
        calcTax(rec);
        return event;
    });



    /**
     * 明細テーブル（商品マスタ）計算
     */
    kintone.events.on(['app.record.create.change.unitPrice', 'app.record.edit.change.unitPrice',
        'app.record.create.change.quantity', 'app.record.edit.change.quantity'
    ], function (event) {
        // 変更レコード取得
        let rec = event.changes.row.value;
        // 金額計算
        rec.price.value = calcAmount(rec.unitPrice.value, rec.quantity.value);
        // 小計計算
        calcSubtotal(event.record);
        // 消費税計算
        calcTax(event.record);

        return event;
    });

    /**
     * 明細テーブル（個別）計算
     */
    kintone.events.on([
        'app.record.create.change.unitPrice_2', 'app.record.create.change.quantity_2',
        'app.record.edit.change.unitPrice_2', 'app.record.edit.change.quantity_2',
    ], function (event) {
        // 変更レコード取得
        let rec = event.changes.row.value;
        // 金額計算
        rec.price_2.value = calcAmount(rec.unitPrice_2.value, rec.quantity_2.value);
        // 小計計算
        calcSubtotal(event.record);
        // 消費税計算
        calcTax(event.record);

        return event;
    });

})(jQuery);
