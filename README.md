# シンプル請求書パック（kintone用アプリテンプレート）

## 概要

シンプル請求書パックは、kintone用アプリテンプレートです。  
会社マスタ、商品マスタ、請求書情報の３つのアプリで、シンプルな請求書情報管理システムになります。 

アプリテンプレートについては[こちら](https://jp.cybozu.help/k/ja/admin/app_admin/template.html)を参照ください。  


## 機能

kintoneで請求書を作成、管理する事ができます。  
請求書の印刷プレビュー画面でカスタマイズされた請求書を出力します。  
紙への印刷、PDFへの保存も勿論可能。  
社判画像を登録すれば印刷時に社判画像を含めた請求書が作成可能。  

## インストール方法

プロジェクトの「APP_TEMPLATE/シンプル請求書パック.zip」をダウンロードして、kintoneに読み込んでご使用ください。  
kintoneへの読み込み方法は[こちら](https://jp.cybozu.help/k/ja/admin/app_admin/template/import_template.html)を参照ください。  
アプリテンプレートからアプリを実際に作成する方法は[こちら](https://jp.cybozu.help/k/ja/user/create_app/app_template.html)を参照ください。