module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly",
        "jQuery": true,
        "moment": true,
        "kintone": true,
        "kintoneUIComponent": true,
        "Encoding": true,
        "Vue": true,
        "invoice": true,
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        // 改行コードはWindows
        "linebreak-style": [
            "error",
            "windows"
        ],
        "indent": [
            0, //無効
            "tab"
        ],
        "no-irregular-whitespace": ['error',
            {
                "skipComments": true,
                "skipTemplates": true
            }
        ],
        "no-unused-vars": 1,
        "no-useless-escape": 1,
        "no-console": "off"
    }
};